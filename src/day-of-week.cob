      * Day of the week - Is there any function in COBOL -IBM Mainframes
      * https://ibmmainframes.com/about9421.html
      *--------------------------------------------------------------
       IDENTIFICATION DIVISION.
       PROGRAM-ID. day-of-week.
       DATA DIVISION.
         WORKING-STORAGE SECTION.
           01 some-fld PICTURE 9 USAGE COMPUTATIONAL.
           01 your-rem PICTURE 9(1) USAGE COMPUTATIONAL.
      *--------------------------------------------------------------
       PROCEDURE DIVISION.
       DIVIDE FUNCTION INTEGER-OF-DATE(20220129) BY 7
       GIVING some-fld REMAINDER your-rem
       EVALUATE your-rem
           WHEN 0 DISPLAY 'SUN'
           WHEN 1 DISPLAY 'MON'
           WHEN 2 DISPLAY 'TUE'
           WHEN 3 DISPLAY 'WED'
           WHEN 4 DISPLAY 'THU'
           WHEN 5 DISPLAY 'FRI'
           WHEN 6 DISPLAY 'SAT'
       END-EVALUATE
       STOP RUN.
