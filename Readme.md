# cobol-playground

Environment to write, run, and debug cobol program.


## Requirements

Dev Containers extension is needed.

- [Dev Containers - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)


## Usage

First, check if the environment works with the sample program.

1. Git clone this repository and open in VSCode.
2. Reopen in container.
3. Open './src/day-of-week.cob' in VSCode, and run it by pressing F5.  
    You can try setting break points.

Once the environment is ready:

1. Delete './src/day-of-week.cob', the sample source code file.
2. Create or place your own cobol source code file to './src/' folder.
3. Playaround by running and debugging as you like.


## Reference

- [COBOL debugger - Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=OlegKunitsyn.gnucobol-debug)
    - VSCode extension. Enables debugging cobol programs in a container.
